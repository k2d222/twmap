use twmap::compression::decompress;
use twmap::datafile::{save, Datafile, Item, RawDatafile};

use std::borrow::Cow;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;

// extracts all data items from a datafile
fn get_data_items(df: &Datafile) -> Vec<Vec<u8>> {
    let mut data_items = Vec::new();
    for (data, size) in df.data_items.iter() {
        data_items.push(decompress(data, *size as usize).unwrap());
    }
    data_items
}

// checks if saving and loading right after changes any data
fn save_load_eq(items: &HashMap<u16, Vec<Item>>, data_items: &[Vec<u8>]) {
    let mut bytes = Vec::new();
    save(
        &mut bytes,
        items,
        &data_items.iter().map(Cow::from).collect::<Vec<_>>(),
    )
    .unwrap();
    let df = RawDatafile::parse(&bytes).unwrap().to_datafile();
    let loaded_items = df.items.clone();
    let loaded_data_items = get_data_items(&df);
    assert_eq!(loaded_items, *items);
    assert_eq!(loaded_data_items, *data_items);
}

#[test]
fn minimal() {
    let items_1 = vec![
        Item {
            id: 0,
            item_data: vec![1, 2, 3],
        },
        Item {
            id: 1,
            item_data: vec![4, 5, 6],
        },
    ];

    let items_2 = vec![
        Item {
            id: 0xffff,
            item_data: vec![0xa, 0xb],
        },
        Item {
            id: 0xeeee,
            item_data: vec![0xc, 0xd],
        },
        Item {
            id: 0xdddd,
            item_data: vec![0xe, 0xf],
        },
    ];
    let mut items = HashMap::new();
    items.insert(5, items_1);
    items.insert(0xbeef, items_2);
    let data_items = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
    save_load_eq(&items, &data_items);
}

#[test]
fn dm1_test() {
    let mut raw = Vec::new();
    let mut dm1 = File::open("tests/dm1.map").unwrap();
    dm1.read_to_end(&mut raw).unwrap();
    let df = RawDatafile::parse(&raw).unwrap().to_datafile();
    let items = df.items.clone();
    let data_items = get_data_items(&df);
    save_load_eq(&items, &data_items);
}

fn bin_eq(path: &str) {
    let mut original_data = Vec::new();
    let mut map_file = File::open(path).unwrap();
    map_file.read_to_end(&mut original_data).unwrap();

    let df1 = RawDatafile::parse(&original_data).unwrap().to_datafile();
    let loaded_items = df1.items.clone();
    let loaded_data_items = get_data_items(&df1);

    let data_items = loaded_data_items.iter().map(Cow::from).collect::<Vec<_>>();
    let mut saved_data = Vec::new();
    save(&mut saved_data, &loaded_items, &data_items).unwrap();

    assert_eq!(original_data, saved_data);
}

#[test]
fn dm1_bin_eq() {
    bin_eq("tests/dm1.map");
}

#[test]
fn editor_map_bin_eq() {
    bin_eq("tests/editor.map");
}
